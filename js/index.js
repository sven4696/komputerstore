import { Laptops } from './laptop.js'

const elImage = document.getElementById('laptop-image')
const elName = document.getElementById('laptop-name')
const elSize = document.getElementById('laptop-size')
const elMemory = document.getElementById('laptop-memory')
const elSpace = document.getElementById('laptop-space')
const elProcessor = document.getElementById('laptop-processor')
const elPrice = document.getElementById('laptop-price')
const elChosenComputer = document.getElementById('select-computer')
const elWorkBtn = document.getElementById('work-btn')
const elBankBtn = document.getElementById('bank-btn')
const elLoanbtn = document.getElementById('loan-btn')
const elBuybtn = document.getElementById('buy-btn')
let elPayAmount = document.getElementById('pay-amount')
let elBalanceAmount = document.getElementById('balance-amount')

let balance = 0; 
let pay = 100; 
let canApplyLoan = true;
let price = 0;

elChosenComputer.addEventListener('change', function(event) {
    const laptop = Laptops.find((currentlaptop) => {
        return currentlaptop.Id == event.target.value;
        
    })
    getPc(laptop)
})

elWorkBtn.addEventListener('click', getPaid);
elBankBtn.addEventListener('click', takeWorkMoney)
elLoanbtn.addEventListener('click', function() {
    let enterLoan = parseInt( window.prompt('How much do you want to loan?'))
    getLoan(enterLoan)
});

elBuybtn.addEventListener('click', function () {
    
    
    buyLaptop(price);

})


// change laptop based on parameters
function getPc(laptop) {
    elImage.src = laptop.Img;
    elName.innerHTML = laptop.Name
    elSize.innerHTML = "Size: " + laptop.Size
    elMemory.innerHTML = "Memory: " + laptop.Ram
    elSpace.innerHTML = "Disk Space: " + laptop.Space
    elProcessor.innerHTML = "Processor: " + laptop.Processor
    elPrice.innerHTML = "Price just " + laptop.Price
}

function getPaid(){
    pay += 100;
    elPayAmount.innerHTML = pay + "$";
}

function takeWorkMoney() {
    balance = balance + pay;
    pay = 0;
    elBalanceAmount.innerHTML = balance + "$"
    elPayAmount.innerHTML = pay + "$";
}

function getLoan(loanAmount) {
    if (loanAmount >= 2*pay){
        window.alert("You cannot loan more than twice your earned money!!")
    }
    else if (!canApplyLoan) {
        window.alert("You cannot apply for anohter loan before you bought a new laptop!")
    }
    else {
        window.alert(`Congrats you have received a loan of: ${loanAmount}`)
        balance = balance + loanAmount
        elBalanceAmount.innerHTML = balance + "$"
        canApplyLoan = false
    }
}

function buyLaptop() {

    price = elPrice.innerHTML
    const price2 = price.match(/\d+/g).map(Number)[0];
    console.log(price2)
    if(balance < price2) {
        window.alert("you have insufficient balance to buy this laptop you have to work some more!")
    }

    else if (balance >= price2)
    {
        window.alert("Congrats you have succesffully purchased the a new laptop")

        balance = balance - price2; 
        elBalanceAmount.innerHTML = balance
        canApplyLoan = true;
    }
   

}




