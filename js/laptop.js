export const Laptops = [
    {
        Id: 1,
        Name: "Mac Book pro",
        Img: "img/macBook.png",
        Size: "16 Inches", 
        Ram: "32 Gb",
        Space: "500 Gb",
        Processor: "i9",
        Price: 1999
    },

    {
        Id: 2,
        Name: "HP office",
        Img: "img/hp.png",
        Size: "12 Inches",
        Ram: "16 Gb",
        Space: "256 Gb",
        Processor: "i7",
        Price: 899
    },

    {
        Id: 3,
        Name: "Huawei X",
        Img: "img/huawei.png",
        Size: "15 Inches",
        Ram: "8 Gb",
        Space: "256 Gb",
        Processor: "i5",
        Price: 499
    },
    {
        Id: 4,
        Name: "HP Proffessional",
        Img: "img/hp2.png",
        Size: "17 Inches",
        Ram: "32 Gb",
        Space: "1000 Gb",
        Processor: "i9",
        Price: 1799
    },
    {
        Id: 5,
        Name: "Asus Gaming beast",
        Img: "img/asus.png",
        Size: "15 Inches",
        Ram: "16 Gb",
        Space: "500 Gb",
        Processor: "i7",
        Price: 999
    }
]